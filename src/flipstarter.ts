import axios from 'axios';
import {
  Campaign,
  fetchFlipstarterDataFromUrl,
  getFlipstarters as getFlipstartersFromDirectory,
} from '@/flipstarterdirectory';
import { ensureHttp } from '@/utils';

const RUNNING = 'running';
const EXPIRED = 'expired';
const SUCCESS = 'success';

// eslint-disable-next-line
let fetched: any = null;

const calculateStatus = (expires: number, txid: string) => {
  // takes milliseconds not seconds
  const timeExpires = new Date(expires * 1000);
  const timeNow = new Date();
  if (txid !== '') {
    return SUCCESS;
  }
  if (timeExpires < timeNow && txid === '') {
    return EXPIRED;
  }
  return RUNNING;
};

const getFlipstarters = async () => {
  if (fetched === null) {
    fetched = await getFlipstartersFromDirectory();
  }
  return fetched;
};

// eslint-disable-next-line
const processCampaignData = (url: string, data: Record<string, any>, description: string) => {
  // eslint-disable-next-line
  const processedData: Campaign = {
    amount: 0,
    url,
    title: '',
    description,
    status: '',
    fundedTx: '',
    categories: [],
    announcements: [],
    archives: [],
  };
  if (data) {
    if (data.campaign) {
      processedData.title = data.campaign.title;
      processedData.status = calculateStatus(
        data.campaign.expires,
        data.campaign.fullfillment_transaction,
      );
      processedData.fundedTx = data.campaign.fullfillment_transaction;
    }
    if (data.recipients) {
      processedData.amount = data.recipients.reduce(
        // eslint-disable-next-line
        (acc: number, rec: Record<string, any>) => acc + rec.recipient_satoshis,
        0,
      );
    }
    return processedData;
  }
  return {};
};

const fetchFromDirectory = async (urlStr: string) => {
  const data = await fetchFlipstarterDataFromUrl(ensureHttp(urlStr));
  if (data && data.campaign) {
    return data.campaign;
  }
  throw Error('Unable to retrieve campaign data.');
};

const fetchFlipstarterData = async (urlStr: string) => {
  const url = new URL(ensureHttp(urlStr));
  const getUrlFragment = (_url: URL) => _url.hash.substring(1);
  const campaignId = getUrlFragment(url) ? parseInt(getUrlFragment(url), 10) : 1;
  // eslint-disable-next-line
  let campaignData: Record<string, any> = {};
  try {
    const response = await axios.get(`${url.origin}/campaign/${campaignId}`);
    campaignData = response.data;
  } catch (error) {
    // very likely because the website doesn't have ssl cert
    // so we fetch from the backend
    if (error.message.includes('Network Error')) {
      return fetchFromDirectory(urlStr);
    }
    // rethrow if we don't attempt to fetchFromDirectory
    throw error;
  }
  let campaignDescription = '';
  try {
    const response = await axios.get(`${url.origin}/static/campaigns/${campaignId}/en/abstract.md`);
    campaignDescription = response.data;
  } catch (error) {
    // ignore
    // at this point the only thing missing is the description
    // that's ok.
    console.log(`could not fetch campaign description from ${urlStr}`);
  }
  // return whatever we managed to get
  // if anything
  return processCampaignData(urlStr, campaignData, campaignDescription);
};

export {
  EXPIRED,
  RUNNING,
  SUCCESS,
  fetchFlipstarterData,
  getFlipstarters,
};
