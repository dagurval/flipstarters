// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer')
//     .BundleAnalyzerPlugin;

/* eslint-disable */
module.exports = {
  publicPath: '',
  chainWebpack: (config) => {
    config
      .plugin('html')
      .tap((args) => {
        args[0].title = 'Flipstarters on Bitcoin Cash';
        return args;
      });
  },
  // configureWebpack: {
  //   plugins: [new BundleAnalyzerPlugin()]
  // },
};
